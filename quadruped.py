# -- coding: utf-8 --
import argparse
import math
import pybullet as p
from time import sleep

dt = 0.01

def init():
    """Initialise le simulateur

    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot
108.6
    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)

    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation

    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    #print(jointInfo)
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints
##############################EVITER LES COLLISIONS#############################
def borner(t,l):
    #t=t%(2*math.pi)
    if(t>l):
        return l
    elif(t<-l):
        return -l
    else: return t

def set_joints(joints,theta0,theta1,theta2,leg_id):
    theta0=borner(theta0,1.76)
    theta1=borner(theta1,2.2)
    theta2=borner(theta2,2.59)
    joints[leg_id*3:leg_id*3+3]=theta0,theta1,theta2
    return joints

###############################POSITION DE REF##################################
def ref():
    return [0.0, 0.6042601844075687, 2.2151819023569947]*4
#####################################LEG_IK#####################################
def inv(x,z):
    L1,L2,L3,L4=40,45,65,87
    a=(-L4**2+z**2+x**2+L3**2)/(2*L3)
    theta1=2*math.atan2(z+math.sqrt(x**2+z**2-a**2),x+a)
    theta2=theta1+math.acos((x-L3*math.cos(theta1))/L4)
    if(z>L3*math.sin(theta1)):
        theta2=theta1-math.acos((x-L3*math.cos(theta1))/L4)
    return theta1,theta2

def leg_ik(x,y,z,leg_id):
    joints = ref()
    theta0= math.atan2(y,x)
    if(x==0):
        l=y
    else:
        l=x/math.cos(theta0)#la longueur entre le premier pivot et le bout de patte.
    if(22>math.sqrt(x**2+y**2+z**2)):
        l=22
    if(math.sqrt(x**2+y**2+z**2)>151):
        l=math.sqrt(151**2-z**2)
    theta1,theta2= inv(l,z)
    set_joints(joints,theta0,theta1,theta2,leg_id)
    return joints

#######################ROBOT_IK#################################################

def robot_ik(x,y,z):
    joints = ref()
    for leg_id in range(4):
        X,Y=chgtRepere(x,y,leg_id)
        X-=50 #pour espacer un peu les pattes
        joints[leg_id*3:leg_id*3+4]=leg_ik(-X,-Y,-z,leg_id)[leg_id*3:leg_id*3+4]
    return joints

def chgtRepere(x,y,leg_id):#x,y dans le repere du robot
    X,Y=[-1,-1,1,1],[1,-1,-1,1]
    m=math.cos(math.pi/4)
    return (x*m*X[leg_id]+y*m*Y[leg_id],
            x*m*X[(leg_id+1)%4]+y*m*Y[(leg_id+1)%4]) #dans le repere de la patte

########################################-WALK-##################################

def interpolate(xs,ts,t):
    if(t<ts[0] or t>ts[-1]):    return 0
    ind=dicho(ts,t,[0,len(ts)])
    if(ind==len(ts)-1):    return xs[-1]#pour pas qu'il y ait d'erreur avec le xs[ind+1]
    m=(xs[ind+1]-xs[ind])*1.0/(ts[ind+1]-ts[ind])#coef directeur de la droite sur le segment où se situe t
    return xs[ind]+m*(t-ts[ind])

def dicho(ts,t,b):#renvoie l'index de t dans ts par dichotomie.
    if (len(ts)<=1):return b[0]
    if(ts[len(ts)//2]>t):
        return dicho(ts[:len(ts)//2],t,[b[0],b[0]+len(ts)//2])
    return dicho(ts[len(ts)//2:],t,[b[0]+len(ts)//2,b[1]])


def walk(xs,ys,rs,t):
    joints=ref()
    if(ys!=0 or xs!=0 or rs!=0):
        cycle=0.75#durée de 4 pas
        t=t%cycle
        T=[0,cycle/8,cycle/4,cycle]
        Z=[-50,20,-50,-50]
        m=(cycle*1000*3/4)
        X=[-m*xs,0,m*xs,-m*xs]
        Y=[-m*ys,0,m*ys,-m*ys]
        for leg_id in range(4):
            Dt=cycle/4*[2,0,3,1][leg_id]#le décalage temporel pour chaque patte
            x,y,z=interpolate(X,T,(t+Dt)%cycle),interpolate(Y,T,(t+Dt)%cycle),interpolate(Z,T,(t+Dt)%cycle)
            x,y=chgtRepere(x,y,leg_id)
            x+=50
            joints[leg_id*3:leg_id*3+4]=leg_ik(x,y,z,leg_id)[leg_id*3:leg_id*3+4]
    return joints

####################################-GOTO-######################################
def goto(x,y,t):
    if(x*y==0):
        xs,ys=0,0
    v=0.05
    if(abs(x)==max(abs(x),abs(y))):
        if(t>x/v):
            return ref()
        xs=v*x/abs(x)
        ys=v*y/abs(x)
    else :
        if(t>y/v):
            return ref()
        ys=v*y/abs(y)
        xs=v*x/abs(y)
    return walk(xs,ys,0,t)

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t = args.m, args.x, args.y, args.t

    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    elif mode == 'leg_ik':
        x = p.addUserDebugParameter("x", -50, 200, 50)
        y = p.addUserDebugParameter("y", -200, 200, 0)
        z = p.addUserDebugParameter("z", -100, 100, -50)
        print('Mode controle des coord de bout de patte...')
    elif mode == 'robot_ik':
        x = p.addUserDebugParameter("x", -100, 200,0)
        y = p.addUserDebugParameter("y", -200, 200, 0)
        z = p.addUserDebugParameter("z", -100, 100, 50)
        print('Mode de controle de la position du robot...')
    elif mode == 'walk':
        xs = p.addUserDebugParameter("x speed", -0.2, 0.2,0)
        ys = p.addUserDebugParameter("y speed", -0.2, 0.2, 0)
        rs = p.addUserDebugParameter("rotation speed", -1, 1, 0)
        print('Mode de controle de vitesse de marche...')
    elif mode == 'goto':
        print('Mode de commande de destination...')
    else:

        raise Exception('Mode non implémenté: %s' % mode)

    t = 0

    # Boucle principale
    while True:
        t += dt

        if mode == 'demo':
            # Récupération des positions cibles
            joints=demo(t, p.readUserDebugParameter(amplitude))
        elif mode == 'leg_ik':
            joints = leg_ik(p.readUserDebugParameter(x),p.readUserDebugParameter(y),p.readUserDebugParameter(z),3)
        elif mode== 'robot_ik':
            joints=robot_ik(p.readUserDebugParameter(x),p.readUserDebugParameter(y),p.readUserDebugParameter(z))
        elif mode=='walk':
            joints=walk(p.readUserDebugParameter(xs),p.readUserDebugParameter(ys),p.readUserDebugParameter(rs),t)
        elif mode=='goto':
            joints=goto(x,y,t)
        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
