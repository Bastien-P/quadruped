# Quadruped

Animation du quadruped réalisé par Bastien Poillion

## Dépendances à installer:    

    pip install numpy scipy pybullet jupyter matplotlib math

## Ensuite:
    python quadruped.py -m mode [-x x-cible -y y-cible -t t-cible]

    Remarque: L'unité de distance est le millimètre

# Objectifs indépendants

## Auto-collisions évidentes
Les auto-collisions les plus évidentes sont évités grace à l'appel de `set_joints` qui borne les angles.
Si un angle vient à dépasser une borne, sa valeur sera alors la valeur de la borne

## Une position stable de référence
La position stable de référence est donnée à chaque début de fonction de mode.
Dans cette position chauque patte a comme coordonnées locale x=50, y=0 et z=-50.

## Adoucissement des mouvements brusques

Pour les fonction `robot_ik` et `leg_ik``j'ai supposé que la valeur des sliders était une valeur continue du temps. Malheureusement des mouvements brusques peuvent apparaître lorsqu'on arrive à des valeurs extrêmes.
Pour la fonction walk, une interpolation linéaire permet de "lisser" le mouvement des pattes, mais il n'y a pas de gestion particulière du changement de vitesse pouvant entrainer une certaine discontinuité.

# Modes  
## mode leg_ik:

leg_ik prend en paramètre les coordonnés cartésiennes du bout d'une patte
(dans le repère de celle-ci), et l'ID de la patte.

Cette fonction calcule $\theta_0$ puis test si les coordonnés passés en argument
sont valides :
    ```
    if(22>math.sqrt(x**2+y**2+z**2) or math.sqrt(x**2+y**2+z**2)>151):
          return joints
    ```

   S'ils le sont les deux autres angles sont calculés avec la fonction inv.
    Sinon, S'arrête approximativement là où elle peut. Toutefois j'ai constaté des incohérences lorsqu'on fait varier es coordonnés d'une patte dans un domaine "interdit". Par exemple si x=z=0 et qu'on fait varier y autour de 0. 

La cinématique inverse (dont les equations résultantes sont dans `inv`)
Donne deux solutions pour les angles (car les calculs impliquent une résolution
      d'équation du second degré en tan( $\theta_1$/2)).
    J'ai choisi la solution ou le coude de la patte était "en haut", ce qui correspond
    à un signe positif devant la racine carrée lors de la définition de $\theta_1$.
## Robot_ik
Pour avancer le corps dans une direction, il suffit de reculer toutes les pattes dans la direction inverse.
Cette fonction appelle pour chaque patte `chgtRepere` qui passe d'un repère global à un repère local des pattes. Ensuite `leg_ik` permet d'ordonner le déplacement de chaque pattes dans la direction opposée de celle donnée au corps du robot en paramètre.

## Walk
Cette fonction permet de contrôler la vitesse de marche du quadrupède dans 2 direction mais ne gère pas la vitesse de rotation.
Pour déplacer le robot l'extêmtité de chaque patte effectue une trajectoire triangle (voir schéma). Pendant qu'une des pattes est en l'air, le bout des trois autres est sur la base du triangle. La phase "en l'air" dure donc trois fois moins longtemps que la phase au sol. Chaque patte effectue le même mouvement mais diphasé d'un tiers de la durée d'un cycle (1 cycle=4 pas).

![schéma triangle](triangle.jpg)

Les coordonnés cartésiennes des sommets de ce triangle sont proportionnelles aux vitesses xs et ys, car la durée d'un cycle est constante. Autrement dit, plus on demande au robot d'aller vite, plus il fait de grand pas.

## Goto
Ce mode fonctionne globalement mais le positionnement final n'est pas très précis.